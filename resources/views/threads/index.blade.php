@extends('layouts.app')

@section('content')
<div class="container">

     <a href="#" class="btn btn-info btn-lg">
          <span class="glyphicon glyphicon-trash"></span> Trash 
        </a>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Threads
                <div class="text-right"><a href="/threads/create">Create new thread</a></div>
                </div>
                <div class="card-body">
                    @foreach($threads as $thread)
                        <article>
                            <div class="row">
                                <div class="col-sm-8">
                                    <h4>
                                        <a href="{{ $thread->path() }}">
                                            {{ $thread->title }}
                                        </a>
                                    </h4>
                                </div>
                                @if(auth()->check() && auth()->user()->id === $thread->user_id)
                                    {{-- you can use this too --}}
                                    {{-- @if(auth()->check() && auth::user()->id === $thread->user_id)  --}}
                                    
                                    <div class="col-sm-2">
                                        <a href="{{ $thread->editThread() }}">Edit</a>
                                    </div>
                                    <div class="col-sm-2">
                                        <form method="POST" action="/threads/{{ $thread->id }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-link">Delete</button>
                                        </form>
                                    </div>
                                @endif
                            </div>
                                
                            <div class="body">{{ $thread->body }}</div>

                          
                        </article>

                        <hr>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
